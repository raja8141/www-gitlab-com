/* global jQuery */
(function($) {
  'use strict';

  $('#team-selector').on('change', function() {
    var selectedDepartmentName = this.value;

    if (selectedDepartmentName === 'All') {
      $('.member').removeClass('hidden');
    } else {
      $('.member').addClass('hidden');

      // show members from department with exact department name from data-department array
      $('.member').each(function() {
        var output = $(this).data('departments') || null;
        if (output && output.constructor === Array) {
          if (output.includes(selectedDepartmentName))
            $(this).removeClass('hidden');
        }
      });
    }
    // append search text in browser URL field as user selects department from dropdown
    history.pushState({}, '', "?department=" + formatString(this.value));

    $('.team-listing').css("display", "flex");
  });

  function getUrlVars(parameter) {
    var href = window.location.href;
    var vars = {};
    if (href.indexOf(parameter) > -1) {
      href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
      });
    }
    return vars;
  }

  // deep check whether an object is empty
  function isObjectEmpty(obj) {
    if (obj == null) return true;
    if (obj.length > 0) return false;
    if (obj.length === 0) return true;
    if (typeof obj !== "object") return true;
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
    }
    return true;
  }

  // factory function for translating text with special characters into usable URL search text
  function formatString(text) {
    if (text && text.length > 0) {
      return text.toLowerCase()
      .replace(/([,-])/g, '')
      .replace(/([//])|([&])|([:])/g, ' ')
      .replace(/\s\s/g, '')
      .replace(/\s/g, '-');
    }
  }

  // accepts, parses search text and change department dropdown option when found
  var urlVar = getUrlVars('department');
  if (!isObjectEmpty(urlVar)) {
    var departmentParam = urlVar['department'] || null;
    var result = $('#team-selector option').filter( function() {
      return formatString($(this).val()) === departmentParam;
    });

    var optionItem = (result && result[0]) ? result[0].value : null;

    if (optionItem) { $('#team-selector')
      .val(optionItem)
      .prop('selected', true)
      .trigger('change');
    }
  } else $('.team-listing').css("display", "flex");

}(jQuery));
