---
layout: markdown_page
title: "History of all-remote work"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

GitLab is an all-remote company with [team members](/company/team/) located in more than 57 countries around the world. 
Check out the [main all-remote page](/company/culture/all-remote/) to learn more about all-remote work. 

On this page, we're curating important historical moments and milestones that have shaped and furthered remote work globally.

### Sir Arthur Charles Clarke

Arthur C. Clarke was a writer, inventor, and futurist. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/FxYgdX2PxyQ?start=104" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In this [BBC Horizon clip](https://youtu.be/FxYgdX2PxyQ?t=104), which originally aired in 1964, Clarke predicts that in or around the year 2000, working remotely by leveraging advanced telecommunications will be possible. A portion of his interview is transcribed below.

"I’m thinking of the incredible breakthrough which has been made possible by developments in communications; particularly the transistor, and above all, the communications satellite. These things will make possible a world which we can be in instant contact with each other, wherever we may be, where we can contact our friends anywhere on Earth, even if we don’t know their actual, physical location.

It will be possible in that age, perhaps only 50 years from now, for a man to conduct his business from Tahiti or Bali just as well as he could from London. In fact if it proves worthwhile, almost any executive skill, any administrative skill, even any physical skill could be made independent of distance. I am perfectly serious when I suggest that one day we may have brain surgeons in Edinburgh operating on patients in New Zealand."

### Robert Noyce

[Robert Noyce](https://www.intel.com/content/www/us/en/history/museum-robert-noyce.html) is known as "the Mayor of Silicon Valley," or "Statesman of Silicon Valley." He is Intel's co-founder and the co-inventor of the integrated circuit.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/VG2jwWtjyXU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In [this Tomorrow/Today clip](https://www.youtube.com/watch?v=VG2jwWtjyXU), which originally aired in March of 1981, Noyce posits that as computing power becomes less expensive and more attainable, it will free workers from the burden of traveling to work. His thoughts have proven prescient, and his foresight is particularly impressive given that it predated the modern internet. A portion of his interview is transcribed below.

"If I look out on the road there, we find most of the cars that are driving by are not carrying goods. They're carrying brains. Trying to take the brain to the place where the work is to be done. With modern communications, and the extension of what we can see in communications, and in computer power — in getting information transferred back and forth — there is no reason why you could not carry on this interview at home, at your office, with me at my office, etcetera.

And I think that as we look farther into the future, we're going to find that people will live where it is conducive to live. Not where it is conducive to work. That the movement of the work to the individual will be much easier, because, as I say, most of our people are doing knowledge work, not work with phsyical materials.

So, at least that half of the population — today's population — could work wherever they please. Without any limitation that they have to go into a particular point to do their work. As long as the communications and the information is available to them wherever they happen to be.

Wouldn't you rather work in Hawaii?"

----

Return to the main [all-remote page](/company/culture/all-remote/).
